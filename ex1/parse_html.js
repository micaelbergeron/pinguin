TEST = "<html><body><h1>Title</h1><a>Lien</a></body></html>";

function parseHTML(html) {    
    var tagRegX = /<([a-z0-9]*)>(.*?)<\/\1>/;        
    // alert(JSON.stringify(matches));
    var output = [];
    while((html.length > 0) && (html.indexOf('<') > -1)) {     
        var matches = tagRegX.exec(html);
        // we find the tag in [1], the content in 2
        var element = {
            tagname: matches[1],
            content: null,
            outerHTML: matches[0],
            innerHTML: matches[2],
            hasChild: matches[2].indexOf('<') > -1
        };        
		
        if (element.hasChild) {
            element.children = parseHTML(element.innerHTML);
        } else {
            element.content = element.innerHTML;
        }
                
        output.push(element);
        html = html.replace(element.outerHTML, "");
    }
    return output;
}

alert(JSON.stringify(parseHTML(TEST)));
