function ajax_fill() {
    var req = new XMLHttpRequest();
    req.open('GET', 'cars.php', true);
    req.onreadystatechange = function (aEvt) {
        if (req.readyState == 4) {
            if (req.status == 200)
                ajax_fill_response(req.responseText);
            else
                dump('error!');
        }
    };
    req.send(null);
}

function ajax_fill_response(data) {
    var oData = JSON.parse(data);
    var table = document.getElementById('cars');

    for (var i in oData) {
        var car = oData[i];
        var row = document.createElement('tr');
        var model = document.createElement('td');
        var make = document.createElement('td');

        model.textContent = car.model;
        make.textContent = car.make;
        row.appendChild(model);
        row.appendChild(make);
        table.appendChild(row);
    }
}

domready(function() {
    ajax_fill();
});