<?php
header('Content-type: application/json');

$cars = array(
     array('model'=>'Elantra', 'make'=>'Hyundai')
    ,array('model'=>'Sonata', 'make'=>'Nissan')
    ,array('model'=>'F-150', 'make'=>'Ford')
);

echo json_encode($cars);

?>