<?php
$cars = array(
     array('model'=>'Elantra', 'make'=>'Hyundai')
    ,array('model'=>'Sonata', 'make'=>'Nissan')
    ,array('model'=>'F-150', 'make'=>'Ford')
);

echo '<table id="cars" border="1" style="width: 300px;">';
echo '<tr><th>Modèle</th><th>Fabriquant</th></tr>';
foreach($cars as $car) {
    $model = $car['model'];
    $make = $car['make'];
    echo "<tr><td>$model</td><td>$make</td></tr>";
}
echo '</table>';

?>