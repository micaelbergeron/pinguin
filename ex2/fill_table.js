function fill_table(tableid) {
    var table = document.getElementById(tableid);
    if (table !== 'undefined') {
        var accum = 1;
        var cells = table.getElementsByTagName('td');
        for (var i in cells) {
            var cell = cells[i];
            cell.innerHTML = "<b>"+accum+"</b>";
            accum = accum + 1;
        }
    }
}

domready(function() {
    fill_table('demo');
});